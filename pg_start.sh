#!/bin/sh

echo "Creating Postgres instance in docker..."
docker run -d \
	--name espg-postgres-instance \
	-p 5432:5432 \
	-e POSTGRES_USER=subroot \
	-e POSTGRES_PASSWORD=espg1234 \
	-e POSTGRES_DB=espg_test \
	postgres:12.2-alpine > /dev/null || exit 1