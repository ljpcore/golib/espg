module gitlab.com/ljpcore/golib/espg

go 1.15

require (
	github.com/lib/pq v1.9.0
	gitlab.com/ljpcore/golib/es v0.2.2
	gitlab.com/ljpcore/golib/test v0.1.2
	gitlab.com/ljpcore/golib/uuid v0.1.1
)
