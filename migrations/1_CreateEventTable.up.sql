CREATE TABLE "ES"."Event" (
	"EventID"        UUID        PRIMARY KEY NOT NULL,
	"StreamID"       UUID                    NOT NULL,
	"SequenceNumber" INTEGER                 NOT NULL,
	"OccurredAt"     TIMESTAMPTZ             NOT NULL,
	"EventType"      TEXT                    NOT NULL,
	"Body"           TEXT                    NOT NULL
);

CREATE UNIQUE INDEX "IndexStreamIDBySequenceNumber" ON "ES"."Event" (
	"StreamID",
	"SequenceNumber"
) INCLUDE (
	"EventType",
	"Body"
);