package espg

const sqlForExpectedSequenceNumber = `
	SELECT "SequenceNumber" + 1 FROM "ES"."Event"
	WHERE "StreamID" = $1
	ORDER BY "SequenceNumber" DESC
	LIMIT 1
`

const sqlForEventInsertion = `
	INSERT INTO "ES"."Event" (
		"EventID",
		"StreamID",
		"SequenceNumber",
		"OccurredAt",
		"EventType",
		"Body"
	) VALUES ($1, $2, $3, $4, $5, $6);
`

const sqlForReadingStream = `
	SELECT
		"EventType",
		"Body"
	FROM
		"ES"."Event"
	WHERE
		"StreamID" = $1
	ORDER BY
		"SequenceNumber";
`
