#!/bin/sh
docker kill espg-postgres-instance > /dev/null
docker rm espg-postgres-instance > /dev/null
echo "Cleaned up Postgres docker container"