package espg

import (
	"context"
	"database/sql"
	"os"
	"testing"

	"gitlab.com/ljpcore/golib/es"
	"gitlab.com/ljpcore/golib/uuid"

	_ "github.com/lib/pq"
	"gitlab.com/ljpcore/golib/test"
	"gitlab.com/ljpcore/golib/test/has"
	"gitlab.com/ljpcore/golib/test/is"
)

func SetupPostgresStore() (*PostgresStore, error) {
	connStr, ok := os.LookupEnv("ESPG_CONN_STR")
	if !ok {
		connStr = "postgres://subroot:espg1234@host.docker.internal:5432/espg_test?sslmode=disable"
	}

	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return nil, err
	}

	if err := db.Ping(); err != nil {
		return nil, err
	}

	inst := es.NewInstantiator()
	inst.AddEventType(&InvoiceCreatedEvent{})
	inst.AddEventType(&InvoiceFeeRaisedEvent{})
	inst.AddEventType(&InvoicePaymentFailedEvent{})
	inst.AddEventType(&InvoicePaymentSucceededEvent{})

	return NewPostgresStore(context.Background(), db, inst), nil
}

func TestCanConnectToTestDatabase(t *testing.T) {
	// Arrange and Act.
	pgs, err := SetupPostgresStore()

	// Assert.
	test.That(t, err, is.Nil)
	test.That(t, pgs, is.NotNil)
}

func TestPostgresStoreWriteToStreamSuccess(t *testing.T) {
	// Arrange.
	pgs, err := SetupPostgresStore()
	test.That(t, err, is.Nil)

	streamID := uuid.New()

	// Act.
	err = pgs.WriteToStream([]es.Event{
		&InvoiceCreatedEvent{
			BaseEvent: es.BaseEvent{
				EventID:        uuid.New(),
				StreamID:       streamID,
				SequenceNumber: 0,
			},
			Amount: 100.00,
		},
		&InvoiceFeeRaisedEvent{
			BaseEvent: es.BaseEvent{
				EventID:        uuid.New(),
				StreamID:       streamID,
				SequenceNumber: 1,
			},
			Amount: 8.00,
		},
	})

	// Assert.
	test.That(t, err, is.Nil)
}

func TestPostgresStoreWriteToStreamSuccessAfterPreviousWrite(t *testing.T) {
	// Arrange.
	pgs, err := SetupPostgresStore()
	test.That(t, err, is.Nil)

	streamID := uuid.New()
	err = pgs.WriteToStream([]es.Event{
		&InvoiceCreatedEvent{
			BaseEvent: es.BaseEvent{
				EventID:        uuid.New(),
				StreamID:       streamID,
				SequenceNumber: 0,
			},
			Amount: 100.00,
		},
		&InvoiceFeeRaisedEvent{
			BaseEvent: es.BaseEvent{
				EventID:        uuid.New(),
				StreamID:       streamID,
				SequenceNumber: 1,
			},
			Amount: 8.00,
		},
	})

	test.That(t, err, is.Nil)

	// Act.
	err = pgs.WriteToStream([]es.Event{
		&InvoiceFeeRaisedEvent{
			BaseEvent: es.BaseEvent{
				EventID:        uuid.New(),
				StreamID:       streamID,
				SequenceNumber: 2,
			},
			Amount: 8.00,
		},
	})

	// Assert.
	test.That(t, err, is.Nil)
}

func TestPostgresStoreWriteToStreamDifferentStreamIDs(t *testing.T) {
	// Arrange.
	pgs, err := SetupPostgresStore()
	test.That(t, err, is.Nil)

	// Act.
	err = pgs.WriteToStream([]es.Event{
		&InvoiceCreatedEvent{
			BaseEvent: es.BaseEvent{
				EventID:        uuid.New(),
				StreamID:       uuid.New(),
				SequenceNumber: 0,
			},
			Amount: 100.00,
		},
		&InvoiceFeeRaisedEvent{
			BaseEvent: es.BaseEvent{
				EventID:        uuid.New(),
				StreamID:       uuid.New(),
				SequenceNumber: 1,
			},
			Amount: 8.00,
		},
	})

	// Assert.
	test.That(t, err, is.NotNil)
}

func TestPostgresStoreWriteToStreamFailureAfterPreviousWriteForSequenceNumber(t *testing.T) {
	// Arrange.
	pgs, err := SetupPostgresStore()
	test.That(t, err, is.Nil)

	streamID := uuid.New()
	err = pgs.WriteToStream([]es.Event{
		&InvoiceCreatedEvent{
			BaseEvent: es.BaseEvent{
				EventID:        uuid.New(),
				StreamID:       streamID,
				SequenceNumber: 0,
			},
			Amount: 100.00,
		},
		&InvoiceFeeRaisedEvent{
			BaseEvent: es.BaseEvent{
				EventID:        uuid.New(),
				StreamID:       streamID,
				SequenceNumber: 1,
			},
			Amount: 8.00,
		},
	})

	test.That(t, err, is.Nil)

	// Act.
	err = pgs.WriteToStream([]es.Event{
		&InvoiceFeeRaisedEvent{
			BaseEvent: es.BaseEvent{
				EventID:        uuid.New(),
				StreamID:       streamID,
				SequenceNumber: 3,
			},
			Amount: 8.00,
		},
	})

	// Assert.
	test.That(t, err, is.NotNil)
}

func TestPostgresStoreReadFromStreamSuccess(t *testing.T) {
	// Arrange.
	pgs, err := SetupPostgresStore()
	test.That(t, err, is.Nil)

	streamID := uuid.New()
	eID1 := uuid.New()
	eID2 := uuid.New()

	err = pgs.WriteToStream([]es.Event{
		&InvoiceFeeRaisedEvent{
			BaseEvent: es.BaseEvent{
				EventID:        eID2,
				StreamID:       streamID,
				SequenceNumber: 1,
			},
			Amount: 8.00,
		},
		&InvoiceCreatedEvent{
			BaseEvent: es.BaseEvent{
				EventID:        eID1,
				StreamID:       streamID,
				SequenceNumber: 0,
			},
			Amount: 100.00,
		},
	})

	test.That(t, err, is.Nil)

	// Act.
	events, err := pgs.ReadFromStream(streamID)

	// Assert.
	test.That(t, err, is.Nil)
	test.That(t, events, has.Length(2))
	test.That(t, events[0].GetEventID(), is.EqualTo(eID1))
	test.That(t, events[1].GetEventID(), is.EqualTo(eID2))
}

func TestPostgresStoreAggregateEndToEnd(t *testing.T) {
	// Arrange.
	pgs, err := SetupPostgresStore()
	test.That(t, err, is.Nil)

	invoice1 := NewInvoice(100)
	invoice1.PaymentSucceeded(25)
	invoice1.FeeRaised(8)
	invoice1.PaymentSucceeded(33)

	// Act.
	err = pgs.WriteFromAggregate(invoice1)
	test.That(t, err, is.Nil)

	invoice2 := &Invoice{}
	err = pgs.ReadToAggregate(invoice1.GetStreamID(), invoice2)
	test.That(t, err, is.Nil)

	// Assert.
	test.That(t, invoice2.BalanceOwing(), is.EqualTo(int64(50)))
	test.That(t, invoice2.GetPendingEvents(), has.Length(0))
}
