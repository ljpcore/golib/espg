![](icon.png)

# espg

An implementation of `es.Store` that uses Postgres as the underlying database.
A Postgres version of 11 or higher is required.

## Migrations

This implementation relies on the existence of the `Event` table in the `ES`
schema.  The migrations in the `migrations` folder can stand this up.  They are
crafted to work with the [`migrate`](https://github.com/golang-migrate/migrate)
tool but should be easily transferrable to other migration tools.

## Local Testing

To run integration tests, ensure you have Docker available and run `./test.sh`
inside the Visual Studio dev container.

This script:
- Stands up Postgres using Docker.
- Waits until it can successfully apply the migrations in the `migrations`
folder to Postgres.
- Runs the full suite of tests.
- Brings down the Postgres instance.

---

Icons made by [Freepik](https://www.flaticon.com/authors/freepik) from
[www.flaticon.com](https://www.flaticon.com/).
