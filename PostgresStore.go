package espg

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"sort"
	"time"

	"gitlab.com/ljpcore/golib/es"
	"gitlab.com/ljpcore/golib/uuid"
)

// PostgresStore is an implementation of es.Store that uses a Postgres table as
// the underlying database.
//
// Use of this type requires a migration to be applied to the Postgres database.
// See the repository README.md for more information.
type PostgresStore struct {
	ctx          context.Context
	db           *sql.DB
	instantiator *es.Instantiator
}

var _ es.Store = &PostgresStore{}

// NewPostgresStore creates a new PostgresStore instance using the provided
// database and event instantiator.
func NewPostgresStore(ctx context.Context, db *sql.DB, instantiator *es.Instantiator) *PostgresStore {
	return &PostgresStore{
		ctx:          ctx,
		db:           db,
		instantiator: instantiator,
	}
}

// ReadFromStream reads all the events from the provided streamID, returning
// them as a slice.
func (s *PostgresStore) ReadFromStream(streamID uuid.UUID) ([]es.Event, error) {
	rows, err := s.db.Query(sqlForReadingStream, streamID)
	if err != nil {
		return nil, fmt.Errorf("failed to read events in stream '%v': %w", streamID, err)
	}
	defer rows.Close()

	events := []es.Event{}
	for rows.Next() {
		var eventType, jsonStr string
		err := rows.Scan(&eventType, &jsonStr)
		if err != nil {
			return nil, fmt.Errorf("failed to read events in stream '%v': %w", streamID, err)
		}

		event, err := s.instantiator.NewEventOfType(eventType)
		if err != nil {
			return nil, fmt.Errorf("failed to read events in stream '%v': %w", streamID, err)
		}

		err = json.Unmarshal([]byte(jsonStr), event)
		if err != nil {
			return nil, fmt.Errorf("failed to read events in stream '%v': %w", streamID, err)
		}

		events = append(events, event)
	}

	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("failed to read events in stream '%v': %w", streamID, err)
	}

	return events, nil
}

// ReadToAggregate reads all the events from the provided streamID, applying
// them to the aggregate as they are read.
func (s *PostgresStore) ReadToAggregate(streamID uuid.UUID, aggregate es.Aggregate) error {
	if aggregate.GetSequenceNumber() != 0 {
		return es.AggregateSequenceNumberNonZeroError(aggregate.GetStreamID())
	}

	events, err := s.ReadFromStream(streamID)
	if err != nil {
		return err
	}

	aggregate.SetStreamID(streamID)
	for _, event := range events {
		aggregate.Apply(event)
	}

	return nil

}

// WriteToStream writes all the provided events to the given stream.
func (s *PostgresStore) WriteToStream(events []es.Event) error {
	tx, err := s.db.BeginTx(s.ctx, nil)
	if err != nil {
		return fmt.Errorf("failed to write events to stream: %w", err)
	}
	defer tx.Rollback()

	streamID, err := ensureEventsShareStreamID(events)
	if err != nil {
		return fmt.Errorf("failed to write events to stream: %w", err)
	}

	expectedSequenceNumber, err := getExpectedSequenceNumberForStream(tx, streamID)
	if err != nil {
		return fmt.Errorf("failed to determine next sequence number for stream '%v': %w", streamID, err)
	}

	if err := ensureValidEventSequencing(expectedSequenceNumber, events); err != nil {
		return fmt.Errorf("failed to write events for stream '%v': %w", streamID, err)
	}

	now := time.Now()
	for _, event := range events {
		if err := insertEvent(tx, now, event); err != nil {
			return fmt.Errorf("failed to write event (%v, seq: %v) to stream '%v': %w", event.Name(), event.GetSequenceNumber(), streamID, err)
		}
	}

	return tx.Commit()
}

// WriteFromAggregate writes all the uncommitted events in the given aggregate
// to its stream.
func (s *PostgresStore) WriteFromAggregate(aggregate es.Aggregate) error {
	events := aggregate.GetPendingEvents()
	if err := s.WriteToStream(events); err != nil {
		return err
	}

	aggregate.ClearPendingEvents()
	return nil
}

func insertEvent(tx *sql.Tx, now time.Time, event es.Event) error {
	rawJSON, err := json.Marshal(event)
	if err != nil {
		return err
	}

	_, err = tx.Exec(sqlForEventInsertion, event.GetEventID(), event.GetStreamID(), event.GetSequenceNumber(), now, event.Name(), string(rawJSON))
	if err != nil {
		return err
	}

	return nil
}

func getExpectedSequenceNumberForStream(tx *sql.Tx, streamID uuid.UUID) (int, error) {
	expectedSequenceNumber := 0

	row := tx.QueryRow(sqlForExpectedSequenceNumber, streamID)
	if err := row.Scan(&expectedSequenceNumber); err != nil {
		if err == sql.ErrNoRows {
			return 0, nil
		}

		return 0, err
	}

	return expectedSequenceNumber, nil
}

func ensureEventsShareStreamID(events []es.Event) (uuid.UUID, error) {
	expectedStreamID := events[0].GetStreamID()
	for _, event := range events {
		if event.GetStreamID() != expectedStreamID {
			return uuid.Empty, es.ConflictingStreamIDError(expectedStreamID, event.GetStreamID())
		}
	}

	return expectedStreamID, nil
}

func ensureValidEventSequencing(expectedSequenceNumber int, events []es.Event) error {
	orderedEvents := make([]es.Event, len(events))
	copy(orderedEvents, events)
	sort.Slice(orderedEvents, func(i, j int) bool {
		return events[i].GetSequenceNumber() < events[j].GetSequenceNumber()
	})

	for _, event := range orderedEvents {
		if event.GetSequenceNumber() != expectedSequenceNumber {
			return es.ConcurrentSequencingError(events[0].GetStreamID(), expectedSequenceNumber, event.GetSequenceNumber())
		}

		expectedSequenceNumber++
	}

	return nil
}
