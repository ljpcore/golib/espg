#!/bin/sh

./pg_start.sh || exit 1
./pg_migrate.sh || exit 1
go test --race --cover ./... || exit 1
./pg_stop.sh