CONN_STR="postgres://subroot:espg1234@host.docker.internal:5432/espg_test?sslmode=disable"
if [ -n "$ESPG_CONN_STR" ]; then
	CONN_STR=$ESPG_CONN_STR
fi

attempt_migration() {
	echo "Attempting migration..."
	MESSAGE=$(migrate --path ./migrations --database "$1" up)
	RESULT=$?

	if [ $RESULT -eq 0 ]; then
		echo $MESSAGE
		return 0
	fi

	return $RESULT
}

DID_MIGRATE=0

for i in $(seq 2 10)
do
	attempt_migration $CONN_STR
	if [ $? -eq 0 ]; then
		echo "-- Migration Succeeded --"
		DID_MIGRATE=1
		break
	fi

	echo "Retrying in $i seconds..."
	sleep $i
done

if [ $DID_MIGRATE -eq 1 ]; then
	exit 0
fi

exit 1