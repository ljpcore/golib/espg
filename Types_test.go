// This source file was taken almost entirely from gitlab.com/ljpcore/golib/es.

package espg

import (
	"errors"
	"fmt"

	"gitlab.com/ljpcore/golib/es"
	"gitlab.com/ljpcore/golib/uuid"
)

type Invoice struct {
	es.BaseAggregate
	amount   int64
	fees     []int64
	payments []int64
}

var _ es.Aggregate = &Invoice{}

func NewInvoice(amount int64) *Invoice {
	a := &Invoice{}
	a.SetStreamID(uuid.New())

	e := &InvoiceCreatedEvent{
		Amount: amount,
	}

	a.AddPendingEvent(e)
	a.Apply(e)

	return a
}

func (a *Invoice) PaymentSucceeded(amount int64) error {
	if a.BalanceOwing() < amount {
		return fmt.Errorf("cannot record payment of %v when balance owing is only %v", amount, a.BalanceOwing())
	}

	e := &InvoicePaymentSucceededEvent{
		Amount: amount,
	}

	a.AddPendingEvent(e)
	a.Apply(e)

	return nil
}

func (a *Invoice) PaymentFailed(amount int64) {
	e := &InvoicePaymentFailedEvent{
		Amount: amount,
	}

	a.AddPendingEvent(e)
	a.Apply(e)
}

func (a *Invoice) FeeRaised(amount int64) error {
	if a.BalanceOwing() < 1 {
		return errors.New("cannot add fee when balance owing is 0")
	}

	e := &InvoiceFeeRaisedEvent{
		Amount: amount,
	}

	a.AddPendingEvent(e)
	a.Apply(e)

	return nil
}

func (a *Invoice) BalanceOwing() int64 {
	x := a.amount

	for _, fee := range a.fees {
		x += fee
	}

	for _, payment := range a.payments {
		x -= payment
	}

	return x
}

func (a *Invoice) Apply(e es.Event) {
	switch x := e.(type) {
	case *InvoiceCreatedEvent:
		a.applyInvoiceCreatedEvent(x)
	case *InvoicePaymentSucceededEvent:
		a.applyInvoicePaymentSucceededEvent(x)
	case *InvoicePaymentFailedEvent:
		a.applyInvoicePaymentFailedEvent(x)
	case *InvoiceFeeRaisedEvent:
		a.applyInvoiceFeeRaisedEvent(x)
	}
}

func (a *Invoice) applyInvoiceCreatedEvent(e *InvoiceCreatedEvent) {
	a.amount = e.Amount
}

func (a *Invoice) applyInvoicePaymentSucceededEvent(e *InvoicePaymentSucceededEvent) {
	a.payments = append(a.payments, e.Amount)
}

func (a *Invoice) applyInvoicePaymentFailedEvent(e *InvoicePaymentFailedEvent) {

}

func (a *Invoice) applyInvoiceFeeRaisedEvent(e *InvoiceFeeRaisedEvent) {
	a.fees = append(a.fees, e.Amount)
}

// --

type InvoiceCreatedEvent struct {
	es.BaseEvent
	Amount int64
}

func (e *InvoiceCreatedEvent) Name() string {
	return "InvoiceCreated"
}

type InvoicePaymentSucceededEvent struct {
	es.BaseEvent
	Amount int64
}

func (e *InvoicePaymentSucceededEvent) Name() string {
	return "InvoicePaymentSucceeded"
}

type InvoicePaymentFailedEvent struct {
	es.BaseEvent
	Amount int64
}

func (e *InvoicePaymentFailedEvent) Name() string {
	return "InvoicePaymentFailed"
}

type InvoiceFeeRaisedEvent struct {
	es.BaseEvent
	Amount int64
}

func (e *InvoiceFeeRaisedEvent) Name() string {
	return "InvoiceFeeRaised"
}
